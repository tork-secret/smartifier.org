<?php


function isAdmin($id){

   $count = User::role('writer')->where('id',$id)->count();
   if ($count>0) {
    return true;
  }else{
    return false;
  }
}

function ConvertSeconds($seconds)
 {
  $dt1 = new DateTime("@0");
  $dt2 = new DateTime("@$seconds");
  return $dt1->diff($dt2)->format('%h:%i:%s');
  }

function AddResult($data){

  \App\Result::insert([
    'right_answer'=>$data->right_answer,
    'wrong_answer'=>$data->wrong_answer,
    'not_answer'=>$data->not_answer,
    'my_marks'=>$data->my_marks,
    'quiz_id'=>$data->quiz_id,
    'times'=>MyTimes($data->quiz_id,Auth::User()->id),
    'user_id'=>Auth::User()->id
  ]);

  // $quiz_data=\App\Quiz::where('id',$data->quiz_id)->select('passing_percentage','is_assesment','title')->first();
  //
  //
  //   $details=[
  //          'title'=>'Mail from Smartifier',
  //          'body'=>'Testing Email'
  //   ];

//
//    if (isset($quiz_data) && $data->my_marks > $quiz_data->passing_percentage) {
//
//     if ($quiz_data->is_assesment==1) {
//
//          \Mail::to(Auth::User()->email)->send(new \App\Mail\TestMail1($details));
//
//     }else{
//            \Mail::to(Auth::User()->email)->send(new \App\Mail\TestMail3($details));
//     }
// }else{
//
//      if ($quiz_data->is_assesment==1) {
//
//          \Mail::to(Auth::User()->email)->send(new \App\Mail\TestMail2($details));
//
//     }else{
//            \Mail::to(Auth::User()->email)->send(new \App\Mail\TestMail4($details));
//     }
//
//
// }


}



function Quizzes(){

   return \App\Quiz::where('deleted_at',null)->where('is_active',1)->orderBy('order','DESC')->get();

}


function OptionsByQuestion($question_id){

   return \App\Option::where('deleted_at',null)->where('question_id',$question_id)->orderBy('order','ASC')->get();

}

function TotalDataByQuiz($quiz_id,$passing_percent){

  $data=new stdClass;

   $questions=\App\Question::where('deleted_at',null)->where('quiz_id',$quiz_id);
   $data->total_marks=$questions->sum('marks');
   $data->total_questions=$questions->count();
   $data->passing_marks=round($data->total_marks*($passing_percent/100));
   return $data;

}


function QuizInfo($quiz_id){

   return \App\Quiz::where('id',$quiz_id)->first();

}


function QuestionInfo($question_id){

   return \App\Question::where('id',$question_id)->first();

}


function MyTimes($quiz_id,$user_id){

   $times=\App\Result::where('quiz_id',$quiz_id)->where('user_id',$user_id)->max('times');

   if ($times>0) {

    return $times+1;

  }else{

    return 1;

  }

}

function QuestionsByQuiz($quiz_id){

  if ($quiz_id!=0) {
    return \App\Question::where('deleted_at',null)->where('quiz_id',$quiz_id)->orderBy('order','ASC')->get();
  }else{
    return \App\Question::where('deleted_at',null)->orderBy('order','ASC')->get();
  }


}


function ShortString($string,$limit,$end_with){

   return \Illuminate\Support\Str::limit($string,$limit,$end_with);

}


 ?>
