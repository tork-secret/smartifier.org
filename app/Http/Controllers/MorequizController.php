<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MorequizController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $quizzes=Quiz::where('is_active',0)->where('deleted_at',null)->get();
        return view('morequiz')->with('quizzes',$quizzes);;
    }
}
