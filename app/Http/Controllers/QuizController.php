<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Question;
use App\Quiz;
use App\User;
use Auth;
use Mail;
use Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class QuizController extends Controller
{

     public function index()
     {
         $quizzes=Quiz::where('is_active',1)->where('deleted_at',null)->where('is_active',1)->orderBy('order','DESC')->get();
         return view('quiz')->with('quizzes',$quizzes);
     }




   function Quiz($quiz_id){

        $quiz=Quiz::where('id',$quiz_id)->where('deleted_at',null)->first();
        return view('single-quiz')->with('quiz',$quiz);

   }
   function StartQuiz($quiz_id){


         $this->quizinfo=Quiz::where('id',$quiz_id)->first();
         $this->questions=Question::inRandomOrder()->where('quiz_id',$quiz_id)->where('deleted_at',null)->get();



        return view('start-quiz')->with('data',$this);

   }

   function QuizSubmit(Request $req){

     // return dd($req->all());

     $right_ans=0;
     $wrong_ans=0;
     $my_marks=0;
     $questions=array();
     $total_ques=count($req->question_id);

     foreach ($req->question_id as $question_id) {
       $right_answer='';
       $title='';


       $question_info=QuestionInfo($question_id);

       if(isset($question_info->right_answer)){
         $right_answer=$question_info->right_answer;
         $title=$question_info->title;

       if($req->input('option_'.$question_id)!=''){



         if($question_info->right_answer==$req->input('option_'.$question_id)){
           $right_ans++ ;

             if(isset($question_info->marks) && $question_info->marks>=0){
               $my_marks=$my_marks+$question_info->marks;
             }

         }else{
           $wrong_ans++ ;
         }
       }

       }

       $question = array(
         'question_id'=>$question_id,
         'my_option' => $req->input('option_'.$question_id),
         'right_answer' =>$right_answer,
         'title' =>$title
        );

       array_push($questions,$question);

     }

     // $result=new Result();
     $this->right_answer=$right_ans;
     $this->wrong_answer=$wrong_ans;
     $this->my_marks=$my_marks;
     $this->quiz_id=$req->quiz_id;
     $this->passing_percentage=$req->passing_percentage;
     $this->questions=$questions;
     //
     // if (Auth::User()) {
     //   $this->user_id=Auth::User()->id;
     //   $this->times=MyTimes($req->quiz_id,Auth::User()->id);
     // }
     $this->not_answer=$total_ques-($wrong_ans+$right_ans);
     // $result->save();

     AddResult($this);

     return redirect()->route('quiz-result')->with('data',$this);

   }

   function QuizResult(){
          return view('quiz-result');
   }



}
