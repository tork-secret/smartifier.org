<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use Auth;
use Mail;
use App\Mail\TestMail;
use App\Mail\TestMail1;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function home()
    {


          if (isset(Auth::User()->getRoleNames()[0]) && Auth::User()->getRoleNames()[0]=='admin') {
            return redirect('/admin/dashboard/');
          }else{


           return view('home');
          }

      }


    public function test()
    {
    // Mail::
    $details=[
           'title'=>'Mail from Smartifier',
           'body'=>'Testing Email'
    ];

    Mail::to('kaniz.ifte@gmail.com')->send(new TestMail($details));
        echo "Email has been sent";
        return view('email.TestMail');
      }
      public function test1()
      {
      // Mail::
      $details=[
             'title'=>'Mail from Smartifier',
             'body'=>'Testing Email'
      ];

      Mail::to('kaniz.ifte@gmail.com')->send(new TestMail1($details));
          echo "Email has been sent 1";

          return view('email.TestMail1');
        }


    public function index()
    {
        // return view('home');
        $quizzes=Quiz::where('is_active',1)->where('deleted_at',null)->get();

       return view('index')->with('quizzes',$quizzes);
    }

    public function morequiz()
    {
        return view('home');
        $quizzes=Quiz::where('is_active',0)->where('deleted_at',null)->get();

       return view('morequiz')->with('quizzes',$quizzes);
    }
}
