<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;

class WelcomeController extends Controller
{

   function index(){


         $quizzes=Quiz::where('is_active',1)->get();

        return view('welcome')->with('quizzes',$quizzes);

   }
}
