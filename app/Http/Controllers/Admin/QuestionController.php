<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Question;
use Auth;

class QuestionController extends Controller
{

  public function __construct()
  {

     $this->middleware(function ($request, $next) {

        if(Auth::User() && isset(Auth::User()->getRoleNames()[0]) && Auth::User()->getRoleNames()[0]=='admin'){

        }else{
          return redirect('/home');
        }
        return $next($request);
    });

  }

   function index(){


         $questions=Question::where('deleted_at',null)->orderBy('order','ASC')->get();


        return view('admin.question.index')->with('questions',$questions);

   }

   function QuestionsByQuiz($quiz_id){


         $questions=Question::where('deleted_at',null)->where('quiz_id',$quiz_id)->orderBy('order','ASC')->get();

        return view('admin.question.index')->with('questions',$questions)->with('quiz_id',$quiz_id);

   }



    public function AddView($quiz_id){

         return view('admin.question.new')->with('quiz_id',$quiz_id);

  }

    public function SimpleAddView(){

         return view('admin.question.new');

  }

   public function AddQuestion(Request $req){



          $question =new Question();
          $question->quiz_id = $req->quiz_id;
          $question->title = $req->title;
          $question->right_answer= $req->right_answer;
          $question->marks= $req->marks;
          $question->order = $req->order ;

          $question->save();




           return redirect('/admin/questions/'.$req->quiz_id);


  }
  public function EditView(Request $req,$id){

      $question=Question::where('id',$id)->first();
     return view('admin.question.update')->with('data',$question);

  }

  public function EditQuestion(Request $req){



          $question = Question::where('id',$req->id)->first();

          $question->quiz_id = $req->quiz_id;
          $question->title = $req->title;
          $question->right_answer= $req->right_answer;
          $question->marks= $req->marks;
          $question->order = $req->order ;

          $question->save();

      return redirect('admin/questions/'.$req->quiz_id);

  }
  public function Delete(Request $req,$id){


       $question = Question::where('id',$req->id)->first();

          $question->deleted_at  = date("Y-m-d H:i:s");

            $question->save();

       return redirect()->back();

  }


}
