<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Option;
use Auth;


class OptionController extends Controller
{

  public function __construct()
  {

     $this->middleware(function ($request, $next) {

        if(Auth::User() && isset(Auth::User()->getRoleNames()[0]) && Auth::User()->getRoleNames()[0]=='admin'){

        }else{
          return redirect('/home');
        }
        return $next($request);
    });

  }


   function index(){


         $options=Option::where('deleted_at',null)->orderBy('order','ASC')->get();

        return view('admin.option.index')->with('options',$options);

   }

   function OptionsByQuestion($question_id){


         $options=Option::where('deleted_at',null)->where('question_id',$question_id)->orderBy('order','ASC')->get();

        return view('admin.option.index')->with('options',$options)->with('question_id',$question_id);

   }

   public function AddView($question_id){

        return view('admin.option.new')->with('question_id',$question_id);

 }

   public function SimpleAddView(){

        return view('admin.option.new');

 }

   public function AddViewByQuestion($question_id){

      return view('admin.option.new')->with('question_id',$question_id);

 }

   public function AddOption(Request $req){



          $option =new Option();
          $option->option = $req->option;
          $option->question_id = $req->question_id;
          $option->order = $req->order ;
          $option->save();




          return redirect('/admin/options/'.$req->question_id);


  }
  public function EditView(Request $req,$id){

      $option=Option::where('id',$id)->first();
     return view('admin.option.update')->with('data',$option);

  }

  public function EditOption(Request $req){





          $option = Option::where('id',$req->id)->first();

          $option->option = $req->option;
          $option->question_id = $req->question_id;
          $option->order = $req->order ;

          $option->save();


       return redirect('/admin/options/'.$req->question_id);

  }
  public function Delete(Request $req,$id){


       $option = Option::where('id',$req->id)->first();

          $option->deleted_at  = date("Y-m-d H:i:s");

            $option->save();

       return redirect()->back();

  }


}
