<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\CollectionExport;
use Illuminate\Http\Request;
use App\Result;
use Auth;
use Excel;
use DB;

class ResultController extends Controller
{

  public function __construct()
  {

     $this->middleware(function ($request, $next) {

        if(Auth::User() && isset(Auth::User()->getRoleNames()[0]) && Auth::User()->getRoleNames()[0]=='admin'){

        }else{
          return redirect('/home');
        }
        return $next($request);
    });

  }

   function index(){


         $results=Result::where('deleted_at',null)->get();

        return view('admin.result.index')->with('results',$results);

   }

   function ExportExcel(){


         $results=DB::table('results')->join('users','results.user_id','users.id')->select('*','results.created_at as created_at')->where('results.deleted_at',null)->get();



         $data[] = array('Name', 'Email','Institute','Partner Club','Quiz/Assessment','Right Ans','Wrong Ans','Not Ans',  'Marks', 'Try No', 'Date');
         foreach($results as $result)
         {

              $quiz_title="";
             if(isset(QuizInfo($result->quiz_id)->title)){
             $quiz_title=QuizInfo($result->quiz_id)->title;
             }
          $data[] = array(

           'Name'  => $result->name,
           'Email'  => $result->email,
           'Institute'  => $result->institute,
           'Partner Club'  => $result->partner,
           'Quiz/Assessment'  => $quiz_title,
           'Right Ans'   => $result->right_answer,
           'Wrong Ans'   => $result->wrong_answer,
           'Not Ans'   => $result->not_answer,
           'Marks'   => $result->my_marks,
           'Try No'   => $result->times,
           'Date'  => $result->created_at,
          );
         }

         return Excel::download(new CollectionExport($data), date("Y-m-d h:i:s").'_result.xlsx');

   }

   public function AddView(){

      return view('admin.result.new');

 }

   public function Addresult(Request $req){

        // $image=$req->image;
        // if($image!=''){
        //      $path="quiz/".Str::random(15).".".$image->getClientOriginalExtension();
        //      $img = Image::make($image);
        //      $img->save($path);
        // }


          $result =new Result();
          $result->user_id = $req->user_id;
          $result->quiz_id = $req->quiz_id;
          $result->right_answer= $req->right_answer;
          $result->wrong_answer= $req->wrong_answer;
          $result->my_marks= $req->my_marks;
          $result->times = $req->times ;

          $result->save();




           return redirect('/admin/result');


  }
  public function EditView(Request $req,$id){

      $result=Result::where('id',$id)->first();
     return view('admin.result.update')->with('data',$result);

  }

  public function EditResult(Request $req){


    // return dd($req->all());

    // $old_image=$req->old_image;
    // $path=$old_image;
    // $image=$req->image;
    //
    //      if($image!=''){
    //
    //         $path="quiz/".Str::random(15).".".$image->getClientOriginalExtension();
    //         $img = Image::make($image);
    //         $img->save($path);
    //         if(File::exists($old_image)){
    //           File::delete($old_image);
    //         }
    //
    //       }
    //


          $result = Result::where('id',$req->id)->first();

          $result->user_id = $req->user_id;
          $result->quiz_id = $req->quiz_id;
          $result->right_answer= $req->right_answer;
          $result->wrong_answer= $req->wrong_answer;
          $result->my_marks= $req->my_marks;
          $result->times = $req->times ;

          $result->save();


       return redirect('/admin/result');

  }
  public function Delete(Request $req,$id){


       $result = result::where('id',$req->id)->first();

          $result->deleted_at  = date("Y-m-d H:i:s");

            $result->save();

       return redirect('/admin/result');

  }


}
