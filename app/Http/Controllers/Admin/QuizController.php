<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Quiz;
use Illuminate\Support\Str;
use DB;
use File;
use Auth;
use Intervention\Image\Facades\Image;

class QuizController extends Controller
{

     public function __construct()
     {

     $this->middleware(function ($request, $next) {

        if(Auth::User() && isset(Auth::User()->getRoleNames()[0]) && Auth::User()->getRoleNames()[0]=='admin'){

        }else{
          return redirect('/home');
        }
        return $next($request);
     });

     }

     public function index(){


           $quizzes=Quiz::where('deleted_at',null)->orderBy('order','DESC')->get();

           // return dd($quizzes);

          return view('admin.quiz.index')->with('quizzes',$quizzes);

     }

     public function AddView(){

          return view('admin.quiz.new');

     }

     public function Addquiz(Request $req){


       $path='';
       $base_badge_path='';


       $image=$req->image;
       $base_badge_image=$req->base_badge;

       if($image!=''){
            $path="uploads/quiz/".Str::random(15).".".$image->getClientOriginalExtension();
            $img = Image::make($image);
            $img->save($path);
       }

       if($base_badge_image!=''){
            $base_badge_path="uploads/badge/".Str::random(15).".".$base_badge_image->getClientOriginalExtension();
            $img = Image::make($base_badge_image);
            $img->save($base_badge_path);
       }


         $quiz =new Quiz();
         $quiz->title = $req->title;
         $quiz->description = $req->description;
         $quiz->image = $path;
         $quiz->base_badge = $base_badge_path;
         $quiz->certificate_title = $req->certificate_title;
         $quiz->start_at = date("Y-m-d H:i:s",strtotime(str_replace('- ','',$req->start_at))) ;
        $quiz->end_at = date("Y-m-d H:i:s",strtotime(str_replace('- ','',$req->end_at))) ;
         $quiz->order = $req->order ;
         $quiz->duration = strtotime($req->duration)-strtotime('00:00:00');

         if($req->passing_percentage==''||$req->passing_percentage<0){
              $quiz->passing_percentage = 75 ;
         }else{
              $quiz->passing_percentage = $req->passing_percentage ;
         }


         if($req->is_active!=null){
              $quiz->is_active = 1 ;
         }else{
              $quiz->is_active = 0 ;
         }

         if($req->is_assesment!=null){
              $quiz->is_assesment = 1 ;
         }else{
              $quiz->is_assesment = 0 ;
         }

         if($req->can_retake!=null){
              $quiz->can_retake = 1 ;
         }else{
              $quiz->can_retake = 0 ;
         }

         $quiz->save();

          return redirect('/admin/quiz');


     }



     public function Edit(Request $req,$id){

     $quiz=Quiz::where('id',$id)->first();
     return view('admin.quiz.update')->with('data',$quiz);

     }



     public function Update(Request $req){



     $path=$req->old_image;
     $base_badge_path=$req->old_base_badge;

     $image=$req->image;
     $base_badge_image=$req->base_badge;

        if($image!=''){

           $path="uploads/quiz/".Str::random(15).".".$image->getClientOriginalExtension();
           $img = Image::make($image);
           $img->save($path);

           if(File::exists($req->old_image)){
             File::delete($req->old_image);
           }

         }

        if($base_badge_image!=''){

           $base_badge_path="uploads/badge/".Str::random(15).".".$base_badge_image->getClientOriginalExtension();
           $img = Image::make($base_badge_image);
           $img->save($base_badge_path);

           if(File::exists($req->old_base_badge)){
             File::delete($req->old_base_badge);
           }

         }



         $quiz = Quiz::where('id',$req->id)->first();

         $quiz->title = $req->title;
         $quiz->description = $req->description;
         $quiz->image = $path;
         $quiz->base_badge = $base_badge_path;
         $quiz->certificate_title = $req->certificate_title;
         $quiz->start_at = date("Y-m-d H:i:s",strtotime(str_replace('- ','',$req->start_at))) ;
         $quiz->end_at = date("Y-m-d H:i:s",strtotime(str_replace('- ','',$req->end_at))) ;
         $quiz->order = $req->order ;
         $quiz->duration = strtotime($req->duration)-strtotime('00:00:00');

         if($req->passing_percentage==''||$req->passing_percentage<0){
              $quiz->passing_percentage = 75 ;
         }else{
              $quiz->passing_percentage = $req->passing_percentage ;
         }


         if($req->is_active!=null){
              $quiz->is_active = 1 ;
         }else{
              $quiz->is_active = 0 ;
         }

         if($req->is_assesment!=null){
              $quiz->is_assesment = 1 ;
         }else{
              $quiz->is_assesment = 0 ;
         }

         if($req->can_retake!=null){

              $quiz->can_retake = 1 ;
         }else{
              $quiz->can_retake = 0 ;
         }
         // return dd($quiz);

         $quiz->save();


      return redirect('/admin/quiz');

     }
     public function Delete(Request $req,$id){


      $quiz = Quiz::where('id',$req->id)->first();

         $quiz->deleted_at  = date("Y-m-d H:i:s");

           $quiz->save();

      return redirect('/admin/quiz');

     }



  }
