<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class DashboardController extends Controller
{

  public function __construct()
  {

    $this->middleware(function ($request, $next) {

    if(Auth::User() && isset(Auth::User()->getRoleNames()[0]) && Auth::User()->getRoleNames()[0]=='admin'){

    }else{
      return redirect('/home');
    }
      return $next($request);
    });

  }

  function index(){

    return view('admin.dashboard.index');

  }


}
