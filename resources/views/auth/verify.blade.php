@extends('layouts.login-app')
@section('content')


<div class="login100-form validate-form p-l-55 p-r-55 p-t-150">
    @csrf
    <span class="login100-form-title">
        {{-- <div class="logo">

                                   <img src="{{asset('assets/images/logo-dark.png')}}">

</div> --}}
<strong>Verify Your Email Address</strong>
</span>

<div class="alert alert-success" role="alert">
    {{ __('A fresh verification link has been sent to your email address.') }}
</div>


@if (session('resent'))
<div class="alert alert-success" role="alert">
    {{ __('A fresh verification link has been sent to your email address.') }}
</div>
@endif

{{ __('Before proceeding, please check your email for a verification link.') }}
{{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}" class="txt3">
    {{ __('click here to request another') }}</a>.
</div>
@endsection
