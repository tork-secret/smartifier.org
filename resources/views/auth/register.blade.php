@extends('layouts.login-app')
@section('content')


<form class="login100-form validate-form p-l-55 p-r-55 p-t-150" method="POST" action="{{ route('register') }}">
    @csrf
    <span class="login100-form-title">
        {{-- <div class="logo">

							<img src="{{asset('assets/images/logo-dark.png')}}">

        </div> --}}
        <strong>Register</strong>
    </span>

    <div class="wrap-input100 validate-input m-b-16">
        <input id="name" type="text" class="input100 {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>
        <span class="focus-input100"></span>

    </div>

    @if ($errors->has('name'))
    <span class="invalid-feedback" role="alert">
        {{ $errors->first('name') }}
    </span>
    @endif

    <div class="wrap-input100 validate-input m-b-16">
        <input id="institute" type="text" class="input100 {{ $errors->has('institute') ? ' is-invalid' : '' }}" name="institute" value="{{ old('institute') }}" placeholder="Institute" required>
        <span class="focus-input100"></span>

    </div>

    @if ($errors->has('institute'))
    <span class="invalid-feedback" role="alert">
        {{ $errors->first('institute') }}
    </span>
    @endif

    <div class="wrap-input100 validate-input m-b-16">
        <input id="partner" type="text" class="input100 {{ $errors->has('partner') ? ' is-invalid' : '' }}" name="partner" value="{{ old('partner') }}" placeholder="Partner Clubs/Organizations">
        <span class="focus-input100"></span>

    </div>

    @if ($errors->has('partner'))
    <span class="invalid-feedback" role="alert">
        {{ $errors->first('partner') }}
    </span>
    @endif

    <div class="wrap-input100 validate-input m-b-16">
        <input id="email" type="email" class="input100 {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
        <span class="focus-input100"></span>

    </div>

    @if ($errors->has('email'))
    <span class="invalid-feedback" role="alert">
        {{ $errors->first('email') }}
    </span>
    @endif

    <div class="wrap-input100 validate-input m-b-16">
        <input id="password" type="password" class="input100 {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
        <span class="focus-input100"></span>


    </div>
    @if ($errors->has('password'))
    <span class="invalid-feedback" role="alert">
        {{ $errors->first('password') }}
    </span>
    @endif


    <div class="wrap-input100 validate-input m-b-16">
        <input id="password-confirm" type="password" class="input100" name="password_confirmation" placeholder="Confirm Password" required>

        <span class="focus-input100"></span>


    </div>


    <div class="container-login100-form-btn">
        <button type="submit" class="login100-form-btn">
            <strong>Register</strong>
        </button>
    </div>

    <div class="flex-col-c p-t-80 p-b-9">
        <span class="txt1 p-b-9">
            Already have an account?
        </span>

        <a href="/login" class="txt3">
            <strong>Sign In</strong>
        </a>
    </div>

    <div class="container-login100-form-btn p-b-30">
        <a href="/" class="login100-form-btn invisible-btn">
            <strong>Visit Website</strong>
        </a>
    </div>

</form>
@endsection
