@extends('layouts.login-app')
@section('content')




<form class="login100-form validate-form p-l-55 p-r-55 p-t-150" method="POST" action="{{ route('password.email') }}">
    @csrf
    <span class="login100-form-title">
        {{-- <div class="logo">

							<img src="{{asset('assets/images/logo-dark.png')}}">

        </div> --}}
        <strong>Reset Password</strong>
    </span>



    @if (session('status'))
    <div class="alert alert-success m-b-16" role="alert">
        {{ session('status') }}
    </div>
    @endif

    <div class="wrap-input100 validate-input m-b-16">
        <input id="email" type="email" class="input100 {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
        <span class="focus-input100"></span>

    </div>

    @if ($errors->has('email'))
    <span class="invalid-feedback" role="alert">
        {{ $errors->first('email') }}
    </span>
    @endif

    <div class="container-login100-form-btn">
        <button type="submit" class="login100-form-btn">
            <strong>Send Password Reset Link</strong>
        </button>
    </div>

    <div class="flex-col-c p-t-80 p-b-9">
        <span class="txt1 p-b-9">
            Go back to login page?
        </span>

        <a href="/login" class="txt3">
            <strong>Login</strong>
        </a>

    </div>

    <div class="container-login100-form-btn p-b-30">
        <a href="/" class="login100-form-btn invisible-btn">
            <strong>Visit Website</strong>
        </a>
    </div>

</form>
@endsection
