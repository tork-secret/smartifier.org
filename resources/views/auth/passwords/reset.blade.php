@extends('layouts.login-app')
@section('content')



<form class="login100-form validate-form p-l-55 p-r-55 p-t-150" method="POST" action="{{ route('password.update') }}">

    @csrf
    <span class="login100-form-title">
        {{-- <div class="logo">

							<img src="{{asset('assets/images/logo-dark.png')}}">

        </div> --}}
        <strong>Reset Password</strong>
    </span>

    <input type="hidden" name="token" value="{{ $token }}">
    <input id="email" type="hidden" class="input100 {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required>

    @if ($errors->has('email'))
    <span class="invalid-feedback" role="alert">
        {{ $errors->first('email') }}
    </span>
    @endif


    <div class="wrap-input100 validate-input m-b-16">
        <input id="password" type="password" class="input100 {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
        <span class="focus-input100"></span>


    </div>
    @if ($errors->has('password'))
    <span class="invalid-feedback" role="alert">
        {{ $errors->first('password') }}
    </span>
    @endif


    <div class="wrap-input100 validate-input m-b-16">
        <input id="password-confirm" type="password" class="input100" name="password_confirmation" placeholder="Confirm Password" required>

        <span class="focus-input100"></span>


    </div>


    <div class="container-login100-form-btn p-b-30">
        <button type="submit" class="login100-form-btn">
            <strong>Reset Password</strong>
        </button>
    </div>

</form>
@endsection
