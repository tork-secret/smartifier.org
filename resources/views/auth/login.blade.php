@extends('layouts.login-app')
@section('content')


<form class="login100-form validate-form p-l-55 p-r-55 p-t-150" method="POST" action="{{ route('login') }}">
	@csrf
	<span class="login100-form-title">
		{{-- <div class="logo">

							<img src="{{asset('assets/images/logo-dark.png')}}">

		</div> --}}
		<strong>Sign In</strong>
	</span>

	<div class="wrap-input100 validate-input m-b-16">
		<input id="email" type="email" class="input100 {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
		<span class="focus-input100"></span>

	</div>

	@if ($errors->has('email'))
	<span class="invalid-feedback" role="alert">
		{{ $errors->first('email') }}
	</span>
	@endif

	<div class="wrap-input100 validate-input">
		<input id="password" type="password" class="input100 {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
		<span class="focus-input100"></span>


	</div>
	@if ($errors->has('password'))
	<span class="invalid-feedback" role="alert">
		{{ $errors->first('password') }}
	</span>
	@endif

	<div class="text-right p-t-13 p-b-23">
		<span class="checkbox txt1">
		    <label>
			   <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
		    </label>
		</span>

		<a href="{{ route('password.request') }}" class="txt2">
			Forgot Password?
		</a>
	</div>

	<div class="container-login100-form-btn">
		<button type="submit" class="login100-form-btn">
			<strong>Sign In</strong>
		</button>
	</div>

	<div class="flex-col-c p-t-80 p-b-9">
		<span class="txt1 p-b-9">
			Don’t have an account?
		</span>

		<a href="/register" class="txt3">
			<strong>Register Now</strong>
		</a>
	</div>

	<div class="container-login100-form-btn p-b-30">
		<a href="/" class="login100-form-btn invisible-btn">
			<strong>Visit Website</strong>
		</a>
	</div>

</form>


@endsection
