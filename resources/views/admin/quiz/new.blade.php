@extends('admin.layouts.app')


@section('meta')
@endsection


@section('css')
@endsection


@section('js')
<!--begin::Page Scripts(used by this page) -->
<script src="{{asset('assets/js/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/bootstrap-timepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/avatar.js')}}" type="text/javascript"></script>
<!--end::Page Scripts -->
@endsection


@section('content-head')

@include('admin.layouts.header.content-head')


@endsection


@section('content')
<style media="screen">
    .text-muted {
        display: none;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Create New quiz
                    </h3>
                </div>
            </div>
            <!--begin::Form-->
            <form method="post" action="{{route('submit-add-quiz')}}" class="kt-form kt-form--fit kt-form--label-right" enctype="multipart/form-data">
                @csrf

                <div class="kt-portlet__body">
                    <div class="form-group row">

                        <label class="col-lg-2 col-form-label">Title</label>
                        <div class="col-lg-3">
                            <input type="text" name="title" class="form-control" placeholder="Enter Title">
                            <span class="form-text text-muted">title</span>
                        </div>

                        <label class="col-lg-2 col-form-label">Image</label>
                        <div class="col-lg-3 col-xl-3">


                            <div class="kt-avatar" id="kt_user_avatar_2">
                                <div class="kt-avatar__holder" style="background-image:url({{asset('assets/images/quiz_banner_sample.png')}})"></div>
                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change Image">
                                    <i class="fa fa-pen"></i>
                                    <input type="file" name="image" accept=".png, .jpg, .jpeg">
                                </label>
                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel Image">
                                    <i class="fa fa-times"></i>
                                </span>
                            </div>
                            <span class="form-text">Recommended Size 400x400</span>
                        </div>

                    </div>
                    <div class="form-group row">



                        <label class="col-lg-2 col-form-label">Description</label>
                        <div class="col-lg-3">
                            <textarea class="form-control" rows="4" name="description"></textarea>
                            <span class="form-text text-muted">Please enter description</span>
                        </div>

                        <label class="col-lg-2 col-form-label">Base Badge</label>
                        <div class="col-lg-3 col-xl-3">


                            <div class="kt-avatar" id="kt_user_avatar_3">
                                <div class="kt-avatar__holder" style="background-image:url({{asset('assets/images/badge_sample.png')}})"></div>
                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change Image">
                                    <i class="fa fa-pen"></i>
                                    <input type="file" name="base_badge" accept=".png, .jpg, .jpeg">
                                </label>
                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel Image">
                                    <i class="fa fa-times"></i>
                                </span>
                            </div>
                            <span class="form-text">Image Must Be 300x300 </span>
                        </div>


                    </div>

                    <div class="form-group row">

                        <label class="col-lg-2 col-form-label">Start At</label>
                        <div class="col-lg-3">

                              <div class="input-group date" >
     						<input type="text" name="start_at" value="{{date("d F Y - h:i A")}}" class="form-control" placeholder="Select Start date and time" id="kt_datetimepicker_5"/>
     						<div class="input-group-append">
     							<span class="input-group-text">
     							<i class="la la-calendar glyphicon-th"></i>
     							</span>
     						</div>
     					</div>



                            <span class="form-text text-muted">Please enter the starting time</span>
                        </div>

                        <label class="col-lg-2 col-form-label">End At</label>
                        <div class="col-lg-3">
                             <div class="input-group date" >
                                  <input type="text" name="end_at" value="{{date("d F Y - ",strtotime("+ 2 day"))}}12:00 AM" class="form-control" placeholder="Select End date and time" id="kt_datetimepicker_5_1"/>
                                  <div class="input-group-append">
                                       <span class="input-group-text">
                                       <i class="la la-calendar glyphicon-th"></i>
                                       </span>
                                  </div>
                             </div>
                        </div>

                    </div>


                    <div class="form-group row">

                         <label class="col-lg-2 col-form-label">Duration</label>
     				<div class="col-lg-3">
     					<div class="input-group timepicker">

     						<input class="form-control is-valid" id="kt_timepicker_1_validate" value="00:00:30" name="duration" readonly placeholder="Select time" type="text"/>

     						<div class="input-group-append">
     							<span class="input-group-text"><i class="la la-clock-o"></i></span>
     						</div>

     					</div>
     					<span class="form-text text-muted">Example help text that remains unchanged.</span>
     				</div>


                        <label class="col-lg-2 col-form-label">Passing Percentage</label>
                        <div class="col-lg-3">
                            <input type="number" name="passing_percentage" value="75" class="form-control" placeholder="Enter Passing marks">
                            <span class="form-text text-muted">Please enter the total marks</span>
                        </div>



                    </div>



                    <div class="form-group row">

                        <label class="col-lg-2 col-form-label">Order</label>
                        <div class="col-lg-3">
                            <input type="number" name="order" class="form-control" placeholder="Enter Order">
                            <span class="form-text text-muted">Please enter Order</span>
                        </div>

                        <label class="col-lg-2 col-form-label">Cetificate Title</label>
                        <div class="col-lg-3">
                            <input type="text" name="certificate_title" class="form-control" placeholder="Cetificate Title">
                            <span class="form-text text-muted">Cetificate Title</span>
                        </div>
                    </div>


                    <div class="form-group row">

                         <label class="col-lg-2 col-form-label"></label>
                         <div class="col-lg-3">

                             <label class="kt-checkbox kt-checkbox--solid kt-checkbox--success">
                                 <input type="checkbox" class="form-control" name="is_active" checked> Active
                                 <span></span>
                             </label>


                             <span class="form-text text-muted">Please enter Order</span>
                         </div>

                         <label class="col-lg-2 col-form-label"></label>

                        <div class="col-lg-3">

                            <label class="kt-checkbox kt-checkbox--solid kt-checkbox--success">
                                <input type="checkbox" class="form-control" name="is_assesment"> Assesment
                                <span></span>
                            </label>


                            <span class="form-text text-muted">Please enter Order</span>
                        </div>


                    </div>



                    <div class="form-group row">

                         <label class="col-lg-2 col-form-label"></label>
                         <div class="col-lg-3">

                             <label class="kt-checkbox kt-checkbox--solid kt-checkbox--success">
                                 <input type="checkbox" class="form-control" name="can_retake" checked> Retake
                                 <span></span>
                             </label>


                             <span class="form-text text-muted">Please enter Order</span>
                         </div>

                    </div>


                </div>





                <div class="kt-portlet__foot kt-portlet__foot--fit-x">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a href="/admin/quiz" class="btn btn-secondary">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
        <!--end::Portlet-->
    </div>

</div>


@endsection
