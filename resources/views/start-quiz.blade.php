@extends('layouts.app')


@php
$quiz=$data->quizinfo;
$total_data=TotalDataByQuiz($quiz->id,$quiz->passing_percentage);
@endphp

@if ($quiz->is_active==1)
@if ( $quiz->start_at < date("Y-m-d H:i:s") && $quiz->end_at>=date("Y-m-d H:i:s") )


@section('css')


@endsection

@section('js')

<script type="text/javascript">

function SubmitQuiz(){
document.getElementById("quiz-form").submit();
}


$(document).ready(function() {

  const measure ='s';

  const timer = $('#timer')
  const s = $(timer).find('.seconds')
  const m = $(timer).find('.minutes')
  const h = $(timer).find('.hours')

  var seconds = 0
  var minutes = 0
  var hours = 0

  var interval = null;

  var clockType = undefined;
  var ammount = 0 ;



  StartStopWatch();

  function StartStopWatch() {


      clockType = 'cronometer'
      if (ammount != '' && measure == 0) {
          alert('Select the Measure')
      } else if (ammount > -1) {
          startClock()
      }
  }



  function pad(d) {
      return (d < 10) ? '0' + d.toString() : d.toString()
  }

  function startClock() {
      hasStarted = false
      hasEnded = false

      seconds = 0
      minutes = 0
      hours = 0

      switch (measure) {
          case 's':
              if (ammount > 3599) {
                  let hou = Math.floor(ammount / 3600)
                  hours = hou
                  let min = Math.floor((ammount - (hou * 3600)) / 60)
                  minutes = min;
                  let sec = (ammount - (hou * 3600)) - (min * 60)
                  seconds = sec
              } else if (ammount > 59) {
                  let min = Math.floor(ammount / 60)
                  minutes = min
                  let sec = ammount - (min * 60)
                  seconds = sec
              } else {
                  seconds = ammount
              }
              break
          case 'm':
              if (ammount > 59) {
                  let hou = Math.floor(ammount / 60)
                  hours = hou
                  let min = ammount - (hou * 60)
                  minutes = min
              } else {
                  minutes = ammount
              }
              break
          case 'h':
              hours = ammount
              break
          default:
              break
      }


      refreshClock()

      $('.input-wrapper').slideUp(350)
      setTimeout(function() {
          $('#timer').fadeIn(350)
          $('#stop-timer').fadeIn(350)

      }, 350)

      switch (clockType) {
          case 'countdown':
              countdown()
              break
          case 'cronometer':
              cronometer()
              break
          default:
              break;
      }
  }



  function pauseClock() {
      clear(interval)
      $('#resume-timer').fadeIn()
      $('#reset-timer').fadeIn()
  }

  var hasStarted = false
  var hasEnded = false
  if (hours == 0 && minutes == 0 && seconds == 0 && hasStarted == true) {
      hasEnded = true
  }



  function cronometer() {
      hasStarted = true
      interval = setInterval(() => {
          if (seconds < 59) {
              seconds++
              refreshClock()
          } else if (seconds == 59) {
              minutes++
              seconds = 0
              refreshClock()
          }

          if (minutes == 60) {
              hours++
              minutes = 0
              seconds = 0
              refreshClock()
          }
          var total_seconds=seconds+minutes*60+hours*60*60;
          var total_duration=0;
          total_duration=<?php echo $quiz->duration; ?> ;

          if(total_seconds>=total_duration){
               pauseClock();
          }

          if (total_duration-total_seconds <= 60 && clockType == 'cronometer') {
              $(timer).find('span').addClass('red')
          }

          if (total_seconds>=total_duration) {
            $('#ConfirmModal').modal('hide');
            $('#TimeoutModal').modal({backdrop: 'static', keyboard: false, show:true});
          }


      }, 1000)
  }

  function refreshClock() {
      $(s).text(pad(seconds))
      $(m).text(pad(minutes))
      if (hours < 0) {
          $(s).text('00')
          $(m).text('00')
          $(h).text('00')
      } else {
          $(h).text(pad(hours))
      }

      if (hours == 0 && minutes == 0 && seconds == 0 && hasStarted == true) {
          hasEnded = true
          alert('The Timer has Ended !')
      }
  }

  function clear(intervalID) {
      clearInterval(intervalID)
      console.log('cleared the interval called ' + intervalID)
  }
});
</script>
@endsection

@section('content')


<div class="single-quiz">
    <div class="container">
        <div class="row">
            <div class="col-md-6 float-left">
                <div class="single-quiz-left box-shadow">
                    <div class="quiz-text">
                        <div class="quiz-title">
                            <h3 class="text-center">{{$quiz->title}}</h3>

                        </div>

                        <div class="quiz-description">
                            <h6 class="text-center">{{$quiz->description}}
                            </h6>

                        </div>
                    </div>
                    <div class="quiz-timer">

                      <div id="timer" >
                        <div class="clock-wrapper object-center">
                            <span class="hours">00</span>
                            <span class="dots">:</span>
                            <span class="minutes">00</span>
                            <span class="dots">:</span>
                            <span class="seconds">00</span>
                        </div>
                    </div>





                    </div>
                </div>
            </div>


            <div class="col-md-6 float-left ">


                <div class="quiz-data-row text-left btn btn-block">
                    <h5>Total Questions : <span class="quiz-data-value">{{$total_data->total_questions}}</span></h5>
                </div>

                <div class="quiz-data-row text-left btn btn-block">
                    <h5>Total Marks : <span class="quiz-data-value">{{$total_data->total_marks}}</span></h5>
                </div>

                <div class="quiz-data-row text-left btn btn-block">
                    <h5>Passing Marks : <span class="quiz-data-value">{{$total_data->passing_marks}} ( {{$quiz->passing_percentage}}% )</span></h5>
                </div>


            </div>


        </div>

    </div>

</div>


<form id="quiz-form" autocomplete="off" action="{{route('quiz-submit')}}" method="get">

    <input type="hidden" name="quiz_id" value="{{$quiz->id}}">
    <input type="hidden" name="passing_percentage" value="{{$quiz->passing_percentage}}">

    <div class="container" id="questions">

        @php
        $serial=0;
        @endphp

        @foreach ($data->questions as $question)

        @php
        $serial++;
        @endphp
        <div class="row">
            <div class="col-md-2 float-left">
            </div>
            <div class="col-md-8 float-left">

                <input type="hidden" name="question_id[]" value="{{$question->id}}">

                <div class="question-row box-shadow">
                <div class="question-title">
                  {{$serial.") ".$question->title}}
                    <br>
                </div>

                <div class="question-options">

                    @php
                    $option_serial=0;
                    @endphp

                    @foreach (OptionsByQuestion($question->id) as $option)


                    @php
                    $option_serial++;
                    @endphp

                    <label class="radio-container"> {{$option->option}}
                        <input type="radio" name="option_{{$question->id}}" value="{{$option_serial}}">
                        <span class="checkmark"></span>
                    </label>

                    @endforeach



                </div>
                </div>

            </div>
            <div class="col-md-2 float-left">

            </div>

        </div>
        <br>
        <br>

        @endforeach

        <div class="row">

            <div class="col-md-2 float-left">
            </div>
            <div class="col-md-8 float-left">

              <div class="object-center">
                  <button type="button" data-toggle="modal" data-target="#ConfirmModal" class="btn min-width-button"> Submit </button>

              </div>

            </div>
            <div class="col-md-2 float-left">
            </div>

        </div>
    </div>
</form>

<!--Confirm Modal -->
<div class="modal fade" id="ConfirmModal" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

      </div>
      <div class="modal-body">
        <img src="{{asset('assets/images/confirm_submit_icon.png')}}" alt="">
        <h4>
             <strong>
             Are You Sure To   Submit Your   @if ($quiz->is_assesment==1) Assessment @else Quiz @endif ?
             </strong>
        </h4>

      </div>
      <div class="modal-footer  object-center">
        <button type="button" class="btn" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn" onclick="SubmitQuiz()">Confirm</button>
      </div>
    </div>
  </div>
</div>

<!-- Timeout Modal -->
<div class="modal fade" id="TimeoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
      </div>
      <div class="modal-body">
        <img src="{{asset('assets/images/timeout_icon.png')}}" alt="">
        <h3> <strong>Your Time’s Up!</strong></h3>
        <br>
        <br>
        <h5> Thank you for taking the    @if ($quiz->is_assesment==1) Assessment @else Quiz @endif ! </h5>
      </div>
      <div class="modal-footer object-center">
        <button type="button" class="btn" onclick="SubmitQuiz()" >Submit  @if ($quiz->is_assesment==1) Assessment @else Quiz @endif</button>
      </div>
    </div>
  </div>
</div>

@endsection

@else

@php
header("Location: /404");
exit;
@endphp


@endif

@else

@php
header("Location: /404");
exit;
@endphp


@endif
