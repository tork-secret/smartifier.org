@extends('layouts.app')

@section('css')
<style media="screen">

@media only screen and (min-width: 992px) {

}
    .py-4 {
        padding-top: 0px !important;
    }

    .quizzes-list .quiz-row {
        display: none;
    }

    .quizzes-list.view-more .quiz-row {
        display: flex !important;
    }

    .quiz-row:first-child,
    .quiz-row:nth-child(2),
    .quiz-row:nth-child(3) {
        display: flex !important;
    }
</style>

@endsection

@section('js')

<script type="text/javascript">
    function LoadMore() {

        $('.quizzes-list').toggleClass('view-more');

        var class_name = $('.quizzes-list').attr('class');

        if (class_name == 'quizzes-list') {

            $('.load-more-button .btn').html("View  More");

        } else {

            $('.load-more-button .btn').html("View  Less");
        }

    }
</script>

@endsection


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-5">
        </div>
        <div class="col-md-7">
            <div class="quizzes-list">

                @php
                $serial=0;
                @endphp

                @foreach ($quizzes as $quiz)
                @php
                $serial++;
                @endphp


                @if ( $quiz->start_at < date("Y-m-d H:i:s") && $quiz->end_at>=date("Y-m-d H:i:s") )


                @php
                $button_icon='heart-full-red.svg';
                @endphp


                <div class="quiz-row active">
                <a href="/quiz/{{$quiz->id}}" class="object-center">


                @else

                @php
                $button_icon='lock_icon.png';
                @endphp

                <div class="quiz-row">
                <a href="javascript:void(0)" class="object-center">
                @endif

                <div class="float-left object-center">
                    <div class="round-icon">

                        <img src="{{asset($quiz->image)}}">

                    </div>
                </div>


                <div class="float-left object-center">
                    <h5 class="quiz-title"> {{$quiz->title}}</h5>
                    <button class="btn  take-quiz-button"> <img src="{{asset('assets/images/'.$button_icon)}}" alt=""> Take The
                        @if ($quiz->is_assesment==1) Assessment
                        @else Quiz @endif</button>
                </div>
                </a>
                </div>
              @endforeach


              @if ($serial>3)

              <div class="load-more-button" onclick="LoadMore()">
                  <button class="btn min-width-button">View More</button>
              </div>
              @endif
          </div>
      </div>
    </div>
  </div>
@endsection
