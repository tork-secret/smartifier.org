@extends('layouts.app')

@section('css')
@endsection

@section('js')

     <script>
     function addTextToImage(imagePath, text) {
         var circle_canvas = document.getElementById("canvas");
         var context = circle_canvas.getContext("2d");

         // Draw Image function
         var img = new Image();
         img.src = imagePath;
         img.onload = function () {
             context.drawImage(img, 0, 0);
             context.lineWidth = 1;
             context.fillStyle = "#000000";
             context.lineStyle = "#333333";
             context.font = "18px Cinderela Personal Use";
             context.fillText(text, 260, 220);
         };
     }

     function addTextToBadge(imagePath, text, text_one, text_two, text_three, text_four, text_certificate_id,text_five, text_six, text_seven) {
                var circle_canvas = document.getElementById("canvas");
                var context = circle_canvas.getContext("2d");
                var context_black = circle_canvas.getContext("2d");


                // Draw Image function
                var img = new Image();
                img.src = imagePath;

                img.onload = function() {



                    context.drawImage(img, 0, 0);
                    context.lineWidth = 1;


                    context.font = "26px Proza Libre";
                    context.fillStyle = "#FFFFFF";
                    context.fillText(text, 205, 125);
                    context.font = "26px Proza Libre";
                    context.fillStyle = "#FFFFFF";
                    context.fillText(text_one, 150, 160);
                    context.font = "26px Proza Libre";
                    context.fillStyle = "#FFFFFF";
                    context.fillText(text_two, 165, 195);
                    context.font = "18px Proza Libre";
                    context.fillStyle = "#000000";
                    context.fillText(text_three, 30, 35);
                    context.font = "18px Proza Libre";
                    context.fillStyle = "#000000";
                    context.fillText(text_four, 355, 35);
                    context.font = "18px Proza Libre";
                    context.fillStyle = "#000000";
                    context.fillText(text_certificate_id, 355, 65);
                    context.font = "20px Proza Libre";
                    context.fillStyle = "#000000";
                    context.fillText(text_five, 25, 415);
                    context.font = "20px Proza Libre";
                    context.fillStyle = "#000000";
                    context.fillText(text_six, 25, 440);
                    context.font = "20px Proza Libre";
                    context.fillStyle = "#000000";

                    context.fillText(text_seven, 25, 467);

                };

            }


     @if(Auth::User())

          @php
               $quiz_info=QuizInfo($data->quiz_id);
          @endphp

          @if (isset($quiz_info->is_assesment) && $quiz_info->is_assesment==1)

               addTextToImage("assets/images/certificate.png", "{{Auth::User()->name}}");
          @else

          addTextToBadge("assets/images/badge.png", "Time", "Management", "for leaders", "date", "Badge ID:", "certificate id","The holder of this badge, Full Name, has ", "successfully passed the quiz on ‘Time ", "Management for leaders’ and scored percent.");

          @endif

     @endif



   </script>

@endsection

@section('content')



<div class="full" style="background-image: url('assets/images/background/quiz_bg.jpg');">



    <div class="container">
        <div class="row">


            <div class="col-md-12 float-left">


                <h1 class="text-center"> Congratulations!!!</h1>

                @if (Auth::User())

                @php
                AddResult($data);
                @endphp
                <br>
                <br>

                <div class="row">

                <div class="col-md-4 float-left">

                </div>

                <div class="col-md-4 float-left">

                     <p>
                         {{PassingText($data->my_marks,$data->quiz_id)}}

                     </p>

                </div>

                <div class="col-md-4 float-left">

                </div>


           </div>




                <br>


                <h2 class="text-center"> Your Score Is </h2>
                <div class="score-section">

                    <div class="text-center badge badge-pill badge-primary">{{$data->my_marks}}
                    </div>

                </div>
            </diV>
            <div class="col-md-6 float-left">

                <div class="card bg-c-pink order-card">
                    <div class="card-block text-center score-card">
                        <h4> Total Questions: <span class="badge  badge-primary"> {{$data->right_answer+$data->wrong_answer+$data->not_answer}} </span></h4>
                        <h4> Right Answers: <span class="badge  badge-primary"> {{$data->right_answer}} </span></h4>
                        <h4> Wrong Answers: <span class="badge  badge-primary">{{$data->wrong_answer}} </span></h4>
                        <h4> Not Answered: <span class="badge  badge-primary"> {{$data->not_answer}} </span></h4>

                    </div>
                </div>
                <br>
                <br>

            </div>

            <div class="col-md-6 float-left">

                 <canvas id="canvas" width="776" height="523"> </canvas>

            </div>

            @else
            <br>
            <br>
            <h2 class="text-center"> To See Your Score Please Login... </h2>

        </div>

        <div class="col-md-3 float-left">
        </div>


        <div class="col-md-6 float-left">


            <form method="POST" action="{{ route('result-login') }}">
                @csrf

                <div class="form-group row">


                    <div class="col-md-12">
                        <input id="email" type="email" placeholder="Enter Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-md-12">
                        <input id="password" placeholder="Enter Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>


                <div class="form-group row mb-0">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Login') }}
                        </button>
                    </div>
                </div>
            </form>
            <br>
            <br>

            <h2>If Not Having Account Please Register..</h2>

            <form method="POST" action="{{ route('result-register') }}">
                @csrf


                <div class="form-group row">
                    <div class="col-md-12">
                        <input id="name" placeholder="Enter Name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>


                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>



                <div class="form-group row">

                    <div class="col-md-12">
                        <input id="institute" placeholder="Enter Institute" type="text" class="form-control @error('email') is-invalid @enderror" name="institute" value="{{ old('institute') }}" required>

                        @error('institute')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-md-12">
                        <input id="partner" placeholder="Partner Clubs/Organizations" type="text" class="form-control @error('partner') is-invalid @enderror" name="partner" value="{{ old('partner') }}" >

                        @error('partner')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-md-12">
                        <input id="email" placeholder="Enter Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-md-12">
                        <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-md-12">
                        <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Register') }}
                        </button>
                    </div>
                </div>
            </form>

            @endif

        </div>

    </div>
</div>



@php
$serial=0;

@endphp



@foreach ($data->questions as $question)

@php
$serial++;
@endphp
<div class="row">
    <div class="col-md-3 float-left">
    </div>
    <div class="col-md-6 float-left">

        <input type="hidden" name="question_id[]" value="{{$question['question_id']}}">

        <div class="question-title">
            <h2>{{$serial.". ".$question['title']}}</h2>
            <br>
        </div>

        <div class="question-options">

            @php
            $option_serial=0;
            @endphp


            @foreach (OptionsByQuestion($question['question_id']) as $option)


            @php
            $option_serial++;
            @endphp

            @if($question['my_option']=='' )

            @php
            $background_color="";
            @endphp

            @elseif ($question['my_option']==$question['right_answer'])

            @php
            $background_color="green";
            @endphp

            @elseif($question['my_option']!=$question['right_answer'])

            @php
            $background_color="red";
            @endphp

            @else
            @php
            $background_color="";
            @endphp

            @endif




            <label class="radio-container" @if ($question['my_option']==$option_serial) style="background:{{$background_color}}" @endif> {{$option->option}}
                <input type="radio" name="option_{{$question['question_id']}}" value="{{$option_serial}}" @if ($question['my_option']==$option_serial) checked @endif>
                    <span class="checkmark"></span>
                    </label>


                    @endforeach



        </div>

    </div>
    <div class="col-md-3 float-left">

    </div>

</div>
<br>
<br>

@endforeach
</div>

@endsection
