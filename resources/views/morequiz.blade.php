@extends('layouts.app')

    @section('css')
    @endsection

    @section('content')
  
    <div id="particles-js">
        <div class="container contain bg_shorthand" >
            <div class=" col-md-2"></div>
            <div class=" col-md-10">
                @foreach ($quizzes as $quiz)
                <br>
                {{-- <div class="panel-body">
    <div class="container"> --}}
                <a href="/start-quiz/{{$quiz->id}}">
                    <div class="row" class="colc">
                        <div class=" col-md-2 float-left">
                            <img src="{{asset($quiz->image)}}" alt="Avatar" class="iii">
                        </div>
                        <div class=" col-md-1"></div>
                        <div class=" col-md-8 float-left">
                            <h1 class="h1_new">{{$quiz->title}}</h1>
                            <p>{{$quiz->description}}</p>
                        </div>
                        {{-- </a> --}}
                    </div>
                </a>
                {{-- </div>

  </div> --}}
                <script src="assets/js/particles.js"></script>
                <script src="assets/js/app.js"></script>
                @endforeach
            </div>
        </div>

<canvas id="canvas" width="340" height="340"></canvas>
<script>
function addTextToImage(imagePath, text) {
  var circle_canvas = document.getElementById("canvas");
  var context = circle_canvas.getContext("2d");

  // Draw Image function
  var img = new Image();
  img.src = imagePath;
  img.onload = function () {
      context.drawImage(img, 0, 0);
      context.lineWidth = 1;
      context.fillStyle = "#CC00FF";
      context.lineStyle = "#ffff00";
      context.font = "18px sans-serif";
      context.fillText(text, 50, 50);
  };
}
addTextToImage("assets/images/blue_bg.jpg", "Hello friends");
</script>
        @endsection
