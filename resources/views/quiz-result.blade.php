@extends('layouts.app')

@section('css')
<style media="screen">
    .roboto-medium{
                font-family: 'Roboto';
                font-weight: 500;
           }
           #canvas-preview{
                border-radius: 10px 10px 30px 30px;
           }
      </style>
@endsection

{{-- @section('js')

<script>
    // function addTextToImage(imagePath, text) {
    //     var circle_canvas = document.getElementById("canvas");
    //     var context = circle_canvas.getContext("2d");
    //
    //     // Draw Image function
    //     var img = new Image();
    //     img.src = imagePath;
    //     img.onload = function () {
    //         context.drawImage(img, 0, 0);
    //         context.lineWidth = 1;
    //         context.fillStyle = "#ffffff";
    //         context.lineStyle = "#333333";
    //         context.font = "18px Cinderela Personal Use";
    //         context.fillText(text, 260, 220);
    //     };
    // }
    //

    //
    //

    //
    //           addTextToImage("assets/images/certificate.png", "");

    //


</script>

@endsection --}}

@section('content')


@if (($data=session()->get('data'))!=null)

@php
$total_data=TotalDataByQuiz($data->quiz_id,$data->passing_percentage);
$quiz_info=QuizInfo($data->quiz_id);

$my_persentage=($data->my_marks/$total_data->total_marks)*100;

$is_passed=0;

if ($data->my_marks>=($total_data->total_marks*($data->passing_percentage/100))) {
$is_passed=1;
}

@endphp

<div class="container">
    <div class="row">
        <div class="col-md-6 float-left">
            <div class="single-quiz-left box-shadow">

                @if ($is_passed==1)


                @if ($quiz_info->is_assesment==1)
                <img src="{{asset('assets/images/success_icon.png')}}" alt="">

                @else
                <div class="canvas-wrapper roboto-medium">



                    <canvas class="hide" id="canvas" width="900" height="900"> </canvas>
                </div>
                <img id="canvas-preview" src="" alt="">

                <br>
                <br>
                <div class="object-center">
                    <button id="download" class="btn">Download</button>
                </div>
                <br>
                <br>
                @section('js')
                <script type="text/javascript">
                    var canvas = document.getElementById("canvas");

                    addTextToBadge("{{$quiz_info->base_badge}}", "{{date('d/m/Y')}}", "{{rand(100000,999999)}}", "{{Auth::User()->name}}   has", "{{$my_persentage}}%.");

                    function ViewImage() {


                        var src = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
                        var img = document.getElementById('canvas-preview');
                        img.setAttribute('src', src);

                    }





                    document.getElementById('download').onclick = function Download() {


                        var link = document.createElement('a');
                        link.setAttribute('download', 'badge.png');
                        link.setAttribute('href', canvas.toDataURL("image/png").replace("image/png", "image/octet-stream"));

                        link.click();

                    }


                    function addTextToBadge(imagePath, date, badge_id, name, persentage) {

                        var context = canvas.getContext("2d");
                        var context_black = canvas.getContext("2d");


                        // Draw Image function
                        var img = new Image();
                        img.src = imagePath;

                        img.onload = function() {


                            context.drawImage(img, 0, 0);
                            context.lineWidth = 1;
                            context.font = "26px 'Roboto'";
                            context.fillStyle = "#ffffff";
                            context.fillText(date, 60, 60);

                            context.font = "26px 'Roboto'";
                            context.fillStyle = "#ffffff";
                            context.fillText(badge_id, 760, 60);
                            context.font = "26px 'Roboto'";
                            context.fillStyle = "#ffffff";
                            context.fillText(name, 400, 780);

                            context.font = "26px 'Roboto'";
                            context.fillStyle = "#ffffff";

                            context.fillText(persentage, 235, 860);

                            ViewImage();


                        };

                    }
                </script>
                @endsection

                @endif

                @else

                <img src="{{asset('assets/images/failure_icon.png')}}" alt="">

                @endif


                <div class="quiz-text">
                    <div class="quiz-title text-center object-center">

                        @if($is_passed==1)

                        <h3 class="text-center">

                            Congratulations!!!


                        </h3>
                        @else



                        <h3 class="text-center">

                            Oh no!


                        </h3>
                        @endif


                    </div>

                    <div class="quiz-description">
                        <h6 class="text-center">

                            @if($is_passed==1)


                            @if ($quiz_info->is_assesment==1)

                            You have passed the assessment with <span class="my_persentage">{{$my_persentage}}%</span> marks! Once your Certificate is ready, we will announce it on our Fight Club. Please be patient and remember to give yourself a
                            reward for this great achievement!
                            <br>
                            <br>

                            P.S. If you think you can do better, maybe give it another try?

                            @else
                            You have passed ‘{{$quiz_info->title}}’. Do remember to share your badge in the comment section.
                            @endif

                            {{-- <span class="my_persentage">{{$my_persentage}}%</span> --}}

                            <br>
                            <br>

                            @else

                            @if ($quiz_info->is_assesment==1)

                            You have scored
                            <span class="my_persentage"> {{$my_persentage}}%
                            </span>
                            marks and need to take the assessment again for the certificate. Turn on your growth mindset, check your notes and your mistakes, and give it another try!

                            @else
                            You have scored
                            <span class="my_persentage"> {{$my_persentage}}%
                            </span>
                            marks and need to take the quiz again for the badge. Turn on your growth mindset, check your notes and your mistakes, and give it another try!

                            @endif



                            <br>
                            <br>

                            <div class="object-center">
                                <a href="/start-quiz/{{$quiz_info->id}}" class="btn">Try Again</a>
                            </div>

                            @endif

                        </h6>

                    </div>
                </div>

            </div>
        </div>


        <div class="col-md-6 float-left ">

            <div class="quiz-data-row text-left btn btn-block">
                <h5>Total Questions : <span class="quiz-data-value">{{$total_data->total_questions}}</span></h5>
            </div>

            <div class="quiz-data-row text-left btn btn-block">
                <h5>Total Marks : <span class="quiz-data-value">{{$total_data->total_marks}}</span></h5>
            </div>

            <div class="quiz-data-row text-left btn btn-block">
                <h5>Passing Marks : <span class="quiz-data-value">{{$total_data->passing_marks}} ( {{$data->passing_percentage}}% )</span></h5>
            </div>

            <div class="quiz-data-row text-left btn btn-block">
                <h5>You Got : <span class="quiz-data-value">{{$data->my_marks}} ( {{$my_persentage}}% )</span></h5>
            </div>



            <div class="quiz-data-row text-left btn btn-block">
                <h5>You Answered : <span class="quiz-data-value">{{$total_data->total_questions-$data->not_answer}}</span></h5>
            </div>

            <div class="quiz-data-row text-left btn btn-block">
                <h5>Right Answered : <span class="quiz-data-value">{{$data->right_answer}}</span></h5>
            </div>

            <div class="quiz-data-row text-left btn btn-block">
                <h5>Wrong Answered : <span class="quiz-data-value">{{$data->wrong_answer}}</span></h5>
            </div>


        </div>


    </div>


    @php
    $serial=0;

    @endphp



    @foreach ($data->questions as $question)

    @php
    $serial++;
    @endphp

    <div class="row m-t-30">
        <div class="col-md-2 float-left">
        </div>
        <div class="col-md-8 float-left">

            <div class="question-row box-shadow">
                <div class="question-title">
                    {{$serial.". ".$question['title']}}
                    <br>
                </div>

                <div class="question-options">

                    @php
                    $option_serial=0;
                    @endphp


                    @foreach (OptionsByQuestion($question['question_id']) as $option)


                    @php
                    $option_serial++;
                    @endphp

                    @if($question['my_option']=='' )

                    @php
                    $background_color="";
                    @endphp

                    @elseif ($question['my_option']==$question['right_answer'])

                    @php
                    $background_color="green";
                    @endphp

                    @elseif($question['my_option']!=$question['right_answer'])

                    @php
                    $background_color="red";
                    @endphp

                    @else
                    @php
                    $background_color="";
                    @endphp

                    @endif




                    <label class="radio-container" @if ($question['my_option']==$option_serial) style="background:{{$background_color}}" @endif> {{$option->option}}
                        <input type="radio" name="option_{{$question['question_id']}}" value="{{$option_serial}}" @if ($question['my_option']==$option_serial) checked @endif>
                            <span class="checkmark"></span>
                            </label>


                            @endforeach



                </div>
            </div>
        </div>

        <div class="col-md-2 float-left">

        </div>

    </div>
    <br>
    <br>

    @endforeach


</div>

@else

@php
header("Location: /404");
exit;
@endphp

@endif

@endsection
