@extends('layouts.app')

@section('css')
@endsection

@section('content')


    <div class="container">
         <div class="xt">
           <h1 class="yt">What fraction of a day is 6 hours?</h1>
           <p class="zt">Choose 1 answer</p>
           <hr />

           <div id='block-11' class="xtt">
             <label for='option-11' class="ytt">
               <input type='radio' name='option' value='6/24' id='option-11' class="mn"/>
               6/24</label>
             <span id='result-11'></span>
           </div>
           <hr />

           <div id='block-12' class="op">
             <label for='option-12'class="ytt" >
               <input type='radio' name='option' value='6' id='option-12' class="mn" />
               6</label>
             <span id='result-12'></span>
           </div>
           <hr />

           <div id='block-13'  class="op">
             <label for='option-13' class="ytt">
               <input type='radio' name='option' value='1/3' id='option-13'class="mn" />
               1/3</label>
             <span id='result-13'></span>
           </div>
           <hr />

           <div id='block-14' class="op">
             <label for='option-14'  class="ytt">
               <input type='radio' name='option' value='1/6' id='option-14' class="mn" />
               1/6</label>
             <span id='result-14'></span>
           </div>
           <hr />
          <a href=""> <button class="button button2"> submit </button></a>

          <div class="row">
               <div class="col-md-4" ></div>
               <div class="col-md-8" class="sumu">
               <a href=""> <button class="button button3"> Previous Page </button></a>
               <a href=""> <button class="button button4"> Next Page </button></a>
          </div>
          </div>
         </div>

         <a id='showanswer1'></a>
</div>
@endsection
