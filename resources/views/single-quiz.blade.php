@extends('layouts.app')

@if ($quiz->is_active==1)

@section('css')
@endsection

@section('meta')


<meta http-equiv="x-ua-compatible" content="ie=edge">

<meta property="og:title" content="@if(isset($quiz->title) && $quiz->title != '') {{$quiz->title}} @endif" />
<meta property="og:type" content="business.business" />
<meta property="og:url" content="{{config('app.url', '')}}" />
<meta property="og:image" content="{{asset($quiz->image)}}" />

<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="600" />

<meta property="og:description" content="{{$quiz->description}} " />



@endsection

@section('content')

  @php
    $total_data=TotalDataByQuiz($quiz->id,$quiz->passing_percentage);
  @endphp


<div class="single-quiz">
    <div class="container">
        <div class="row">
            <div class="col-md-6 float-left">
            <div class="single-quiz-left box-shadow">
                <div class="quiz-text">
                <div class="quiz-title">
                    <h3 class="text-center">{{$quiz->title}}</h3>

                </div>

                <div class="quiz-description">
                    <h6 class="text-center">{{$quiz->description}}
                    </h6>

                </div>
                </div>
                <div class="quiz-banner">

                    <img src="{{asset($quiz->image)}}">

                </div>
            </div>
            </div>


            <div class="col-md-6 float-left ">
                <div class="quiz-data-row text-left btn btn-block">
                    <h5>Start at : <span class="quiz-data-value">
                      @if (date('Y')==date("Y",strtotime($quiz->start_at)))

                        {{date("F j , g:i A",strtotime($quiz->start_at))}}

                      @else
                        {{date("F j , Y, g:i A",strtotime($quiz->start_at))}}
                      @endif

                    </span></h5>
                </div>

                <div class="quiz-data-row text-left btn btn-block">
                    <h5>Total Questions : <span class="quiz-data-value">{{$total_data->total_questions}}</span></h5>
                </div>

                <div class="quiz-data-row text-left btn btn-block">
                    <h5>Total Marks : <span class="quiz-data-value">{{$total_data->total_marks}}</span></h5>
                </div>

                <div class="quiz-data-row text-left btn btn-block">
                    <h5>Passing Marks : <span class="quiz-data-value">{{$total_data->passing_marks}} ( {{$quiz->passing_percentage}}% )</span></h5>
                </div>

                <div class="quiz-data-row text-left btn btn-block">
                    <h5>
                      Duration:
                      <span class="quiz-data-value">
                      @if (gmdate('H', $quiz->duration)>0)
                        {{gmdate('H', $quiz->duration)}} Hour
                        @endif
                        @if (gmdate('i', $quiz->duration)>0)
                        {{gmdate('i', $quiz->duration)}} Min
                        @endif
                        @if (gmdate('s', $quiz->duration)>0)
                        {{gmdate('s', $quiz->duration)}} Sec
                        @endif


                    </span>
                  </h5>
                </div>


                @if ( $quiz->start_at < date("Y-m-d H:i:s") && $quiz->end_at>=date("Y-m-d H:i:s") )

                     <div class="start-now-button object-center">

                         <a href="/start-quiz/{{$quiz->id}}" class="btn min-width-button">Start Now</a>

                     </div>
                @endif

            </div>


        </div>


    </div>

</div>
<div class="container">
  <div class="row">
      <div class="col-md-12">

          <h2 class="text-center"> Share on </h2>

          <div class="share-icon">
              <a href="https://www.facebook.com/sharer.php?u={{url()->current()}}" target="_blank">
                  <img src="{{asset('assets/images/facebook_icon.png')}}" alt="Facebook" class="icons">
              </a>
              <a href="http://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}" target="_blank">
                  <img src="{{asset('assets/images/linkedin_icon.png')}}" alt="LinkedIn" class="icons">
              </a>
          </div>

      </div>
  </div>

</div>

@endsection

@else

@php
header("Location: /404");
exit;
@endphp


@endif
