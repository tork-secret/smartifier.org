<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index')->name('index');
Route::get('/home','HomeController@home')->name('home');
Route::get('/quiz','QuizController@index')->name('quiz');
Route::get('/quiz/{quiz_id}', 'QuizController@Quiz')->name('quiz');
Route::get('/result', function () {
    return view('result');
});

Auth::routes();

Route::group(['middleware' => 'auth' ], function(){

Route::get('/start-quiz/{quiz_id}', 'QuizController@StartQuiz')->name('strat-quiz');
Route::get('/quiz-result', 'QuizController@QuizResult')->name('quiz-result');
Route::get('/quiz-submit', 'QuizController@QuizSubmit')->name('quiz-submit');

});


Route::get('/admin/login', 'Auth\LoginController@LoginView');
Route::get('/admin/dashboard','Admin\DashboardController@index')->name('dashboard');


Route::get('/admin/quiz', 'Admin\QuizController@index')->name('quiz');
Route::get('/admin/add-quiz', 'Admin\QuizController@AddView')->name('add-quiz');
Route::post('/admin/add-quiz', 'Admin\QuizController@Addquiz')->name('submit-add-quiz');
Route::get('/admin/edit-quiz/{id}', 'Admin\QuizController@Edit')->name('edit-quiz');
Route::post('/admin/update-quiz', 'Admin\QuizController@Update')->name('update-quiz');
Route::get('/admin/delete-quiz/{id}', 'Admin\QuizController@Delete')->name('dalete-quiz');


Route::get('/admin/options/{question_id}', 'Admin\OptionController@OptionsByQuestion')->name('options-by-question');
Route::get('/admin/options', 'Admin\OptionController@index')->name('option');
// Route::get('/admin/add-option', 'Admin\OptionController@AddView')->name('add-option');
Route::get('/admin/add-option/{question_id}', 'Admin\OptionController@AddView')->name('add-option');
Route::get('/admin/add-option', 'Admin\OptionController@SimpleAddView')->name('simple-add-option');
Route::post('/admin/add-option', 'Admin\OptionController@AddOption')->name('submit-add-option');
Route::get('/admin/edit-option/{id}', 'Admin\OptionController@EditView')->name('edit-option');
Route::post('/admin/edit-option', 'Admin\OptionController@EditOption')->name('submit-edit-option');
Route::get('/admin/delete-option/{id}', 'Admin\OptionController@Delete')->name('delete-option');


Route::get('/admin/questions/{quiz_id}', 'Admin\QuestionController@QuestionsByQuiz')->name('questions-by-quiz');
Route::get('/admin/questions', 'Admin\QuestionController@index')->name('question');
Route::get('/admin/add-question/{quiz_id}', 'Admin\QuestionController@AddView')->name('add-question');
Route::get('/admin/add-question', 'Admin\QuestionController@SimpleAddView')->name('simple-add-question');
Route::post('/admin/add-question', 'Admin\QuestionController@AddQuestion')->name('submit-add-question');
Route::get('/admin/edit-question/{id}', 'Admin\QuestionController@EditView')->name('edit-question');
Route::post('/admin/edit-question', 'Admin\QuestionController@EditQuestion')->name('submit-edit-question');
Route::get('/admin/delete-question/{id}', 'Admin\QuestionController@Delete')->name('delete-question');


Route::get('/admin/result', 'Admin\ResultController@index')->name('result');
Route::get('/admin/add-result', 'Admin\ResultController@AddView')->name('add-result');

Route::post('/admin/add-result', 'Admin\ResultController@AddResult')->name('submit-add-result');
Route::get('/admin/edit-result/{id}', 'Admin\ResultController@EditView')->name('edit-result');
Route::post('/admin/edit-result', 'Admin\ResultController@EditResult')->name('submit-edit-result');
Route::get('/admin/delete-result/{id}', 'Admin\ResultController@Delete')->name('delete-result');
Route::get('/admin/result/export-excel', 'Admin\ResultController@ExportExcel')->name('result-export-excel');
